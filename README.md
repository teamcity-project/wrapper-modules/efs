# EFS wrapper module for Teamcity project

This module is wrapper for [terraform-aws-efs by lazzurs](https://github.com/lazzurs/terraform-aws-efs) module

The module creates:
  - EFS resources
  - Create security group for access to EFS

## Requirements
Terraform  0.12

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

No provider.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| efs\_name | The name of the Elastic File System | `string` | n/a | yes |
| encrypted | Turn encryption on or off | `bool` | `true` | no |
| environment | Environment name | `string` | `""` | no |
| region | AWS region | `string` | n/a | yes |
| subnet\_ids | Subnet IDs for Mount Targets | `list(string)` | n/a | yes |
| tags | A map of tags to add to all resources | `map(string)` | `{}` | no |
| vpc\_id | The ID of the VPC that EFS will be deployed to | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| arn | EFS ARN |
| dns\_name | EFS DNS name |
| id | EFS ID |
| mount\_target\_ids | List of EFS mount target IDs (one per Availability Zone) |
| security\_group\_arn | EFS Security Group ARN |
| security\_group\_id | EFS Security Group ID |
| security\_group\_name | EFS Security Group name |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## To Do
* Use more powerful basic module