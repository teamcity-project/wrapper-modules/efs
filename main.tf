terraform {
  # The configuration for this backend will be filled in by Terragrunt or via a backend.hcl file. See
  # https://www.terraform.io/docs/backends/config.html#partial-configuration
  backend "s3" {}

  # Only allow this Terraform version. Note that if you upgrade to a newer version, Terraform won't allow you to use an
  # older version, so when you upgrade, you should upgrade everyone on your team and your CI servers all at once.
  required_version = "~> 0.12"

  required_providers {
    aws = "~> 2.20"
  }
}

provider "aws" {
  region = var.region
}

module "efs" {
  # TODO: use more powerful module
  source  = "lazzurs/efs/aws"
  version = "0.3.0"

  vpc_id     = var.vpc_id
  efs_name   = var.efs_name
  subnet_ids = var.subnet_ids

  tags = merge(var.tags, map("Environment", format("%s", var.environment)))
}
