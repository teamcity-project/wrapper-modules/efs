output "arn" {
  value       = module.efs.arn
  description = "EFS ARN"
}

output "id" {
  value       = module.efs.id
  description = "EFS ID"
}

output "dns_name" {
  value       = module.efs.dns_name
  description = "EFS DNS name"
}

output "security_group_id" {
  value       = module.efs.security_group_id
  description = "EFS Security Group ID"
}

output "security_group_arn" {
  value       = module.efs.security_group_arn
  description = "EFS Security Group ARN"
}

output "security_group_name" {
  value       = module.efs.security_group_name
  description = "EFS Security Group name"
}

output "mount_target_ids" {
  value       = module.efs.security_group_name
  description = "List of EFS mount target IDs (one per Availability Zone)"
}
