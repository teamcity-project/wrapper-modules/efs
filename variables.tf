variable "region" {
  description = "AWS region"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
  default     = ""
}

variable "subnet_ids" {
  description = "Subnet IDs for Mount Targets"
  type        = list(string)
}

variable "encrypted" {
  description = "Turn encryption on or off"
  type        = bool
  default     = true
}

variable "efs_name" {
  description = "The name of the Elastic File System"
  type        = string
}

variable "vpc_id" {
  description = "The ID of the VPC that EFS will be deployed to"
  type        = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
